# PROJETO DESAFIO CONTROL
## Desenvolvedor: Joedson Gabriel | json.gbriel@gmail.com


#### Status: 

- ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) Em andamento (   )
- ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Concluído ( X )


### Objetivo: Desenvolver um sistema de acordo com as instruções fornecidas para participação do processo seletivo da 'Control Construções - Engenharia Elétrica e Civil'
Manual https://drive.google.com/file/d/15PtBU5QlI_fiOIBb_c80INFAs5-f2Prp/view?usp=sharing

### Considerações iniciais
 Resalto que este projeto foi separado em duas partes: Front-end e Back-end a fim de proporcionar maior escalabilidade ao sistema
 
 Backend: https://bitbucket.org/JsonGbriel/desafio_control
 
 Frontend: https://bitbucket.org/JsonGbriel/desafio_control_front/
 
 
 
 Também foi realizada a separação do repositório de testes para fins de organização:
 
 Testes automatizados: https://bitbucket.org/JsonGbriel/desafio_control_tests/
 

### Aplicação
 O deploy deste projeto foi realizado utilizando plataformas opensource podendo ser acessado atravez das informações abaixo
 
 Endereços de acesso da aplicação:
 
 - Frontend:  https://control-challenge.netlify.app/
 - Backend:  https://control-challenge.herokuapp.com/
 - API:  https://control-challenge.herokuapp.com/api/

 
### Sobre o projeto
 Este sistema foi desenvolvido utilizando as seguintes tecnologias
 
 - Backend: Django (Python)
 - API: Django Rest Framework com JWT (JSON Web Token)
 - Banco de dados: PostgreSQL (produção) | SQLite (local)
 - Frontend: Vuejs (versão CLI)
 - Biblioteca visual frontend: Bootstrap Vue
 - Deploy frontend: Netlify
 - Deploy backend: Heroku
 - Testes automatizados: Selenium WebDriver (Python)

### Testes automatizados
 Para execução dos testes automatizados se faz necessário que realize a instalação de todas as dependências que estão presentes no arquivos requirements.txt do repositório de testes.
 
 Certifique-se também do Python instalado e configurado em sua máquina.
 
 Os testes podem ser encontrados dentro da pasta tests no repositório de testes. 
 
 Cada testes pode ser executado individualmente (login, service, service_order). 
 
 
 repositório de testes: https://bitbucket.org/JsonGbriel/desafio_control_tests/
