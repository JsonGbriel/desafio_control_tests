from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from settings import URL_BASE
import time

if __name__ == "__main__":
    browser = webdriver.Chrome()
    browser.get(URL_BASE)

    time.sleep(2)

    username = browser.find_element_by_xpath('/html/body/div/div[2]/div/div/div/form/div[2]/input')
    password = browser.find_element_by_xpath('/html/body/div/div[2]/div/div/div/form/div[3]/input')
    submit = browser.find_element_by_xpath('/html/body/div/div[2]/div/div/div/form/button')

    username.send_keys('admin')
    password.send_keys('admin')

    submit.click()

    time.sleep(2)

    try:
        assert 'home' in browser.current_url
        print('TEST SUCCESS')
    except Exception as e:
        print('TEST ERROR')
        print(e)

    browser.quit()