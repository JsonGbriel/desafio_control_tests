from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from settings import URL_BASE
import time, random

if __name__ == "__main__":
    browser = webdriver.Chrome()
    browser.get(URL_BASE)

    time.sleep(3.5)

    username = browser.find_element_by_xpath('/html/body/div/div[2]/div/div/div/form/div[2]/input')
    password = browser.find_element_by_xpath('/html/body/div/div[2]/div/div/div/form/div[3]/input')
    submit = browser.find_element_by_xpath('/html/body/div/div[2]/div/div/div/form/button')

    username.send_keys('admin')
    password.send_keys('admin')

    submit.click()

    time.sleep(3.5)

    try:
        assert 'home' in browser.current_url
    except Exception as e:
        print('TEST ERROR')
        print(e)
        browser.quit()

    browser.get(URL_BASE + 'service-order')

    time.sleep(3.5)

    add = browser.find_element_by_xpath('/html/body/div/div[2]/div[1]/div[2]/button')
    add.click()

    time.sleep(3.5)

    service = browser.find_element_by_xpath('/html/body/div[2]/div[1]/div/div/div/form/div/div[2]')
    service.click()
    time.sleep(3.5)
    first_option = browser.find_element_by_xpath('/html/body/div[2]/div[1]/div/div/div/form/div/div[3]/ul/li[1]/span')
    first_option.click()

    time.sleep(3.5)

    qtd = browser.find_element_by_xpath("//input[@type='number']")
    qtd.send_keys(random.randint(1,100))

    employee = browser.find_element_by_xpath("//input[@name='employee']")
    employee.send_keys('auto create')

    date = browser.find_element_by_xpath("//input[@name='date']")
    date.send_keys('value', '10052021')

    start = browser.find_element_by_xpath("//input[@name='start']")
    start.send_keys('value', '10052021')
    start.send_keys(Keys.TAB)
    start.send_keys('0800')

    end = browser.find_element_by_xpath("//input[@name='end']")
    end.send_keys('value', '10052021')
    end.send_keys(Keys.TAB)
    end.send_keys('1800')

    detail = browser.find_element_by_xpath("//textarea[@name='detail']")
    detail.send_keys('auto create')

    time.sleep(3.5)

    save_btn = browser.find_element_by_xpath("//button[@type='submit']")
    save_btn.click()

    time.sleep(2)

    try:
        message = browser.find_element_by_xpath('/html/body/div[2]/div/div/div/div')
        assert 'A ordem de serviço foi cadastrada com êxito.' in message.text
        # print('TEST SUCCESS')
    except Exception as e:
        print('TEST ERROR')
        print(e)
        browser.quit()

    time.sleep(3.5)

    edit_btn = browser.find_elements_by_class_name("btn-primary")
    edit_btn[1].click()

    time.sleep(2)

    employee = browser.find_element_by_xpath("//input[@name='employee']")
    employee.send_keys(' (edited)')

    save_btn = browser.find_element_by_xpath("//button[@type='submit']")
    save_btn.click()

    time.sleep(3.5)

    try:
        message = browser.find_element_by_xpath('/html/body/div[2]/div/div/div/div')
        assert 'A ordem de serviço foi cadastrada com êxito.' in message.text
        # print('TEST SUCCESS')
    except Exception as e:
        print('TEST ERROR')
        print(e)
        browser.quit()

    print('iniciando delete')

    time.sleep(3.5)

    del_btn = browser.find_elements_by_class_name('btn-danger')
    del_btn[0].click()

    time.sleep(3.5)

    try:
        message = browser.find_element_by_xpath('/html/body/div[2]/div/div/div/div')
        assert 'A ordem de serviço foi excluída com êxito.' in message.text
        print('TEST SUCCESS')
    except Exception as e:
        print('TEST ERROR')
        print(e)
        browser.quit()

    browser.quit()