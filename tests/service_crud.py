from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from settings import URL_BASE
import time, random

if __name__ == "__main__":
    browser = webdriver.Chrome()
    browser.get(URL_BASE)

    time.sleep(3.5)

    username = browser.find_element_by_xpath('/html/body/div/div[2]/div/div/div/form/div[2]/input')
    password = browser.find_element_by_xpath('/html/body/div/div[2]/div/div/div/form/div[3]/input')
    submit = browser.find_element_by_xpath('/html/body/div/div[2]/div/div/div/form/button')

    username.send_keys('admin')
    password.send_keys('admin')

    submit.click()

    time.sleep(3.5)

    try:
        assert 'home' in browser.current_url
    except Exception as e:
        print('TEST ERROR')
        print(e)
        browser.quit()

    browser.get(URL_BASE + 'service')

    time.sleep(3.5)

    add = browser.find_element_by_xpath('/html/body/div/div[2]/div[1]/div[2]/button')
    add.click()

    time.sleep(3.5)

    rand_value = str(round(random.uniform(1, 100), 2))

    description = browser.find_element_by_xpath("//input[@type='text']")
    description.send_keys('auto create')

    value = browser.find_element_by_xpath("//input[@type='number']")
    value.send_keys(rand_value)

    save_btn = browser.find_element_by_xpath("//button[@type='submit']")
    save_btn.click()

    time.sleep(3.5)

    try:
        message = browser.find_element_by_xpath('/html/body/div[2]/div/div/div/div')
        assert 'O serviço foi cadastrado com êxito.' in message.text
        # print('TEST SUCCESS')
    except Exception as e:
        print('TEST ERROR')
        print(e)
        browser.quit()

    edit_btn = browser.find_element_by_xpath('/html/body/div/div[2]/div[2]/div/div/table/tbody/tr/td[3]/button[1]')
    edit_btn.click()

    time.sleep(3.5)

    description = browser.find_element_by_xpath("//input[@type='text']")
    description.send_keys(' (edited)')

    save_btn = browser.find_element_by_xpath("//button[@type='submit']")
    save_btn.click()

    time.sleep(3.5)

    try:
        message = browser.find_element_by_xpath('/html/body/div[2]/div/div/div/div')
        assert 'O serviço foi cadastrado com êxito.' in message.text
        # print('TEST SUCCESS')
    except Exception as e:
        print('TEST ERROR')
        print(e)
        browser.quit()

    print('iniciando delete')

    time.sleep(3.5)

    del_btn = browser.find_elements_by_class_name('btn-danger')
    del_btn[0].click()

    time.sleep(3.5)

    try:
        message = browser.find_element_by_xpath('/html/body/div[2]/div/div/div/div')
        assert 'O serviço foi excluído com êxito.' in message.text
        print('TEST SUCCESS')
    except Exception as e:
        print('TEST ERROR')
        print(e)
        browser.quit()

    browser.quit()